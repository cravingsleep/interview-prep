import { Node } from '../tree';

/**
 * A lucky tree is any tree where no two parent -> child nodes
 * add up to 13. Null nodes have a val of '0'.
 */
function isLucky(tree: Node<number> | null): boolean {
    if (tree === null) {
        return true;
    }

    const isNodeLucky = tree.val + (tree.left?.val || 0) !== 13
        && tree.val + (tree.right?.val || 0) !== 13;

    return isNodeLucky && isLucky(tree.left) && isLucky(tree.right);
}

export { isLucky };
