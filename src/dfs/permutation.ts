function permutations(arr: number[]): number[][] {
    if (arr.length === 0) {
        return [];
    }

    if (arr.length === 1) {
        return [[arr[0]]];
    }

    const perms = arr.flatMap((num, i) => {
        return permutations(arr.filter((_, idx) => i !== idx))
            .map(arr => [num].concat(arr));
    });

    return perms;
}

export { permutations };
