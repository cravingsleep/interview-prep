import { Node } from '../tree';

function visibleNodes(node: Node<number> | null): number {
    function dfs(node: Node<number> | null, currentMax: number): number {
        if (node === null) {
            return 0;
        }

        const isNodeVisible = node.val >= currentMax ? 1 : 0;
        const newCurrentMax = Math.max(node.val, currentMax);

        return isNodeVisible + dfs(node.left, newCurrentMax) + dfs(node.right, newCurrentMax);
    }

    return dfs(node, Number.NEGATIVE_INFINITY);
}

export { visibleNodes };
