import { Node } from '../tree';

function isBalanced(tree: Node<number> | null): boolean {
    function dfsHeight(tree: Node<number> | null): number {
        if (tree === null) {
            return 0;
        }

        const leftHeight = dfsHeight(tree.left);
        if (leftHeight === -1) {
            return -1;
        }
        const rightHeight = dfsHeight(tree.right);
        if (rightHeight === -1) {
            return -1;
        }

        if (Math.abs(leftHeight - rightHeight) > 1) {
            return -1;
        }

        return Math.max(leftHeight, rightHeight) + 1;
    }

    return dfsHeight(tree) !== -1;
}

export { isBalanced };
