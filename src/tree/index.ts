type Node<T> = {
    readonly val: T,
    readonly left: Node<T> | null,
    readonly right: Node<T> | null
}

function serialise(tree: Node<number> | null): string {
    if (tree === null) {
        return 'x';
    }

    return [String(tree.val)]
        .concat(serialise(tree.left))
        .concat(serialise(tree.right))
        .join(' ');
}

function deserialise(s: string): Node<number> | null {
    if (s.length === 0) {
        return null;
    }

    const cs = s.split(' ');

    function dfs(chrs: string[]): Node<number> | null {
        const val = chrs.shift();
        
        if (val === null || val === 'x') {
            return null;
        }

        const left = dfs(chrs);
        const right = dfs(chrs);

        return {
            val: parseInt((val || '0'), 10),
            left,
            right
        };
    }

    return dfs(cs);
}


export { Node, serialise, deserialise };
