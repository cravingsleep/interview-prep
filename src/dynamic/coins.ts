function leastCoinsUsed(value: number, coins: number[]): number {
    if (coins.length === 0) {
        return -1;
    }

    const coinsToReachValue = new Map<number, number>([[0, 0]]);
    
    for (let i = 1; i <= value; i++) {
        coins
            .forEach(coin => {
                const diff = i - coin;
                
                if (diff >= 0) {
                    coinsToReachValue.set(i, (coinsToReachValue.get(diff) || 0) + 1);
                }
            });
    }

    return coinsToReachValue.get(value) || -1;
}

function leastCoinsUsedPath(value: number, coins: number[]): number[] {
    if (coins.length === 0) {
        return [];
    }

    const coinsToReachValue = new Map<number, number[]>([[0, []]]);
    
    for (let i = 1; i <= value; i++) {
        coins
            .forEach(coin => {
                const diff = i - coin;
                
                if (diff >= 0) {
                    coinsToReachValue.set(i, (coinsToReachValue.get(diff) || []).concat(coin));
                }
            });
    }

    return coinsToReachValue.get(value) || [];
}


export { leastCoinsUsed, leastCoinsUsedPath };

