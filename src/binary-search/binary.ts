function binarySearch(sorted: number[], n: number): boolean {
    if (sorted.length === 0) {
        return false;
    }

    const pivot = Math.floor(sorted.length / 2);
    const pivotItem = sorted[pivot];

    if (n === pivotItem) {
        return true;
    }

    if (n < pivotItem) {
        return binarySearch(sorted.slice(0, pivot), n);
    }

    return binarySearch(sorted.slice(pivot + 1, sorted.length), n);
}

function binarySearchIterative(sorted: number[], n: number): boolean {
    if (sorted.length === 0) {
        return false;
    }

    let start = 0;
    let end = sorted.length;

    while (start <= end) {
        const pivot = Math.floor((start + end) / 2);
        const pivotItem = sorted[pivot];

        if (n === pivotItem) {
            return true;
        }
        
        if (n < pivotItem) {
            end = pivot - 1;
        } else {
            start = pivot + 1;
        }
    }

    return false;
}

export { binarySearch, binarySearchIterative };
