function medianTwoArraysTwoPointer(arr1: number[], arr2: number[]): number {
    let left = 0;
    let right = 0;

    const fullLength = arr1.length + arr2.length;
    const medianPosition = Math.ceil(fullLength / 2);

    const items: number[] = [];

    while (items.length <= medianPosition) {
        const leftItem = arr1[left] || Number.POSITIVE_INFINITY;
        const rightItem = arr2[right] || Number.POSITIVE_INFINITY;

        if (leftItem < rightItem) {
            items.push(leftItem);
            left++;
        } else {
            items.push(rightItem);
            right++;
        }
    }

    if (fullLength % 2 === 1) {
        return items[medianPosition - 1];
    }

    return (items[medianPosition - 1] + items[medianPosition]) / 2;
}

function median(arr: number[]): { middlePoint: number, median: number } {
    if (arr.length === 0) {
        return  { middlePoint: 0, median: 0 };
    }

    const middlePoint = Math.floor(arr.length / 2);

    if (arr.length % 2 === 0) {
        // [1, 2, (3 middlePoint), 4]
        return { middlePoint, median: (arr[middlePoint - 1] + arr[middlePoint]) / 2 };
    }

    return { middlePoint, median: arr[middlePoint] };
}

export { medianTwoArraysTwoPointer };
