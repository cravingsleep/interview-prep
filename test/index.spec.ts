import { add } from '../src';

describe('Add', () => {
    it('should add two numbers', () => {
        expect(add(10, 20)).toBe(30);
    });
});
