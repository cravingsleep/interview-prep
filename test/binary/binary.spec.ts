import { binarySearch, binarySearchIterative } from '../../src/binary-search/binary';

describe('Binary Search', () => {
    [{
        name: 'Recursive',
        func: binarySearch
    }, {
        name: 'Iterative',
        func: binarySearchIterative
    }].forEach(({ name, func: binarySearch }) => {
        describe(name, () => {
            it('should return false for an empty array', () => {
                expect(binarySearch([], 1)).toBe(false);
            });

            it('should search one item', () => {
                expect(binarySearch([1], 1)).toBe(true);
            });

            it('should search two items', () => {
                expect(binarySearch([1, 5], 5)).toBe(true);
            });

            it('should search multiple items', () => {
                expect(binarySearch([1, 2, 5, 10, 20, 50, 100, 101], 50)).toBe(true);
            });

            it('should fail to find in multiple items', () => {
                expect(binarySearch([1, 2, 5, 10, 20, 50, 100, 101], 51)).toBe(false);
            });
        });
    });
});
