import { medianTwoArraysTwoPointer } from '../../src/binary-search/median';

describe('Median of Two Sorted Arrays', () => {
    [{
        name: 'medianTwoArraysTwoPointer',
        func: medianTwoArraysTwoPointer
    }, 
    // {
    //     name: 'medianBinary',
    //     func: medianBinary
    // }
    ].forEach(({ name, func: medianTwoArrays }) => {
        describe(name, () => {
            it('should find the middle median of one array', () => {
                expect(medianTwoArrays([], [1, 2, 3])).toBe(2);
            });

            it('should find the division median of one array', () => {
                expect(medianTwoArrays([], [1, 2, 3, 4])).toBe(2.5);
            });

            it('should find the division median of two arrays', () => {
                // 1 2 3 5 8 10
                expect(medianTwoArrays([2, 8, 10], [1, 3, 5])).toBe(4);
            });

            it('should find the middle median of two arrays', () => {
                // 1 2 3 5 8 10 11
                expect(medianTwoArrays([2, 8, 10, 11], [1, 3, 5])).toBe(5);
            });
        });
    });
});
