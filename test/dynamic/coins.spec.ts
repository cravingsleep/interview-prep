import { leastCoinsUsed, leastCoinsUsedPath } from '../../src/dynamic/coins';

describe('Coins Used', () => {
    describe('Amount', () => {
        it('should identify it is impossible with no coins', () => {
            expect(leastCoinsUsed(1, [])).toBe(-1);
        });

        it('should identify it is impossible with a value lower than the coins', () => {
            expect(leastCoinsUsed(2, [3, 5, 10])).toBe(-1);
        });

        it('should identify one coin is the answer', () => {
            expect(leastCoinsUsed(10, [3, 5, 10])).toBe(1);
        });

        it('should identify two coins is the answer', () => {
            expect(leastCoinsUsed(15, [3, 5, 10])).toBe(2);
        });

        it('should identify three coins is the answer', () => {
            expect(leastCoinsUsed(9, [1, 3, 7])).toBe(3);
        });

        it('should identify a complex case', () => {
            expect(leastCoinsUsed(1001, [1, 3, 7, 13])).toBe(77);
        });
    });

    describe('Path', () => {
        it('should identify it is impossible with no coins', () => {
            expect(leastCoinsUsedPath(1, [])).toEqual([]);
        });

        it('should identify it is impossible with a value lower than the coins', () => {
            expect(leastCoinsUsedPath(2, [3, 5, 10])).toEqual([]);
        });

        it('should identify one coin is the answer', () => {
            expect(leastCoinsUsedPath(10, [3, 5, 10])).toEqual([10]);
        });

        it('should identify two coins is the answer', () => {
            expect(leastCoinsUsedPath(15, [3, 5, 10])).toEqual([5, 10]);
        });

        it('should identify three coins is the answer', () => {
            expect(leastCoinsUsedPath(9, [1, 3, 7])).toEqual([1, 1, 7]);
        });
    });
});
