import { visibleNodes } from '../../src/dfs/visible';
import { deserialise } from '../../src/tree';

describe('Visible Nodes', () => {
    it('should return 0 for no tree', () => {
        expect(visibleNodes(null)).toBe(0);
    });

    it('should return 1 for just a root', () => {
        expect(visibleNodes({ val: 1, left: null, right: null })).toBe(1);
    });

    it('should return the right amount of visible for a complex tree', () => {
        const tree = deserialise('5 4 3 x x 8 x x 6 x x');

        expect(visibleNodes(tree)).toBe(3);
    });
});
