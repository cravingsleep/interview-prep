import { isBalanced } from '../../src/dfs/balanced';
import { deserialise } from '../../src/tree';

describe('Balanced', () => {
    it('should return true for no tree', () => {
        expect(isBalanced(null)).toBe(true);
    });

    it('should return true for a single node', () => {
        expect(isBalanced({ val: 1, left: null, right: null })).toBe(true);
    });

    it('should return true for a complex balanced tree', () => {
        const tree = deserialise('1 2 4 x 7 x x 5 x x 3 x 6 x x');
        expect(isBalanced(tree)).toBe(true);
    });

    it('should return false for a complex unbalanced tree', () => {
        const tree = deserialise('1 2 4 x 7 x x 5 x x 3 x 6 8 x x x');
        expect(isBalanced(tree)).toBe(false);
    });
});
