import { permutations } from '../../src/dfs/permutation';

describe('Permutations', () => {
    it('should handle the empty array', () => {
        expect(permutations([])).toEqual([]);
    });

    it('should handle a single item', () => {
        expect(permutations([1])).toEqual([
            [1]
        ]);
    });

    it('should handle the complex case', () => {
        expect(permutations([1, 2, 3])).toEqual([
            [1, 2, 3],
            [1, 3, 2],
            [2, 1, 3],
            [2, 3, 1],
            [3, 1, 2],
            [3, 2, 1]
        ]);
    });

    it('should handle the duplicate case', () => {
        expect(permutations([1, 2, 2])).toEqual([
            [1, 2, 2],
            [1, 2, 2],
            [2, 1, 2],
            [2, 2, 1],
            [2, 1, 2],
            [2, 2, 1]
        ]);
    });
});
