import { isLucky } from '../../src/dfs/lucky';
import { deserialise } from '../../src/tree';

describe('Lucky', () => {
    it('should return true for no tree', () => {
        expect(isLucky(null)).toBe(true);
    });

    it('should return true for a single node that is not 13', () => {
        expect(isLucky({ val: 1, left: null, right: null })).toBe(true);
    });

    it('should return false for a single node that is 13', () => {
        expect(isLucky({ val: 13, left: null, right: null })).toBe(false);
    });

    it('should return true for a two nodes adding up to not 13', () => {
        const tree = deserialise('1 11 x x x');
        expect(isLucky(tree)).toBe(true);
    });

    it('should return false for two nodes adding up to 13', () => {
        const tree = deserialise('1 x 12 x x');
        expect(isLucky(tree)).toBe(false);
    });

    it('should return true for a complex lucky tree', () => {
        const tree = deserialise('1 2 4 x 7 x x 5 x x 3 x 6 8 x x x');
        expect(isLucky(tree)).toBe(true);
    });

    it('should return false for a complex unlucky tree', () => {
        const tree = deserialise('1 2 4 x 9 x x 5 x x 3 x 6 8 x x x');
        expect(isLucky(tree)).toBe(false);
    });
});
