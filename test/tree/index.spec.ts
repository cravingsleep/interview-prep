import { deserialise, serialise } from '../../src/tree';

describe('Tree Operations', () => {
    describe('Serialisation', () => {
        it('should serialise an empty string', () => {
            expect(serialise(null)).toEqual('x');
        });

        it('should serialise a single node', () => {
            expect(
                serialise({ val: 1, left: null, right: null })
            ).toEqual('1 x x');
        });

        it('should serialise a complex tree', () => {
            const complex = {
                val: 1,
                left: { val: 2, left: null, right: null },
                right: { val: 3, left: null, right: null }
            };

            expect(
                serialise(complex)
            ).toEqual('1 2 x x 3 x x');
        });
    });

    describe('Deserialisation', () => {
        it('should deserialise an empty string', () => {
            expect(deserialise('')).toEqual(null);
        });

        it('should deserialise a single node', () => {
            expect(
                deserialise('1 x x')
            ).toEqual({ val: 1, left: null, right: null });
        });

        it('should deserialise a complex tree', () => {
            const complex = {
                val: 1,
                left: { val: 2, left: null, right: null },
                right: { val: 3, left: null, right: null }
            };

            expect(
                deserialise('1 2 x x 3 x x')
            ).toEqual(complex);
        });
    });
});
